module.exports = {
    '@tags' : ['all','google'],
	'Checks google search is present' : function(browser){

		browser.url(browser.globals.baseUrl )
		.useXpath()
		.waitForElementVisible('//*[@id="lga"]', 5000);
	
		browser.expect.element('//*[@id="lga"]').to.be.present;

	},
	'Perform a search' : function(browser){
	
		browser.searchFor(browser.globals.to_search);
		
	},
	'Check all option is active' : function(browser){
	
		browser.pause(5000)
		.assert.containsText('//*[@id="top_nav"]/div/div/div[1]/div/div/div[1]', "All");
		
		browser.expect.element('//*[@id="top_nav"]/div/div/div[1]/div/div/div[1]').to.have.css('color').which.equals('rgba(66, 133, 244, 1)');
		
		browser.end();
		
	}
};