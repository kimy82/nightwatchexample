var Utils = module.exports = {};
var moment = require('moment');

Utils.getTomorrowFormatedDate = function(){
	var date = new Date();
	var dateString = (date.getDate()+1) + '.' + (date.getMonth()+1) + '.' + date.getFullYear();
	return dateString;
};

Utils.isDateExpired = function(inputDate){
	var date = new Date();
	var todayMoment = moment(date);
	var deadLineDateMoment = moment(inputDate, "YYYY.MM.DD HH:mm");
	
	return todayMoment.isBefore(deadLineDateMoment);
};