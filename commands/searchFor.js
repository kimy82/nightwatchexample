var Utils = require('./lib/Utils.js');

exports.command = function(searchword) {

	this.useXpath().waitForElementVisible('//*[@id="searchform"]/form/div[2]/div[2]/div[1]/div[1]/div[3]/div/div[3]/div/input[1]', 10000)
	.setValue('//*[@id="searchform"]/form/div[2]/div[2]/div[1]/div[1]/div[3]/div/div[3]/div/input[1]', searchword + Utils.getTomorrowFormatedDate())
	.sendKeys('//*[@id="searchform"]/form/div[2]/div[2]/div[1]/div[1]/div[3]/div/div[3]/div/input[1]', this.Keys.ENTER);
	
	return this;
	
}