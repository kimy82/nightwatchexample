#NODE NIGHTWATCH UI SELENIUM TEST

##TUTORIAL GOAL

**Build fast and easy UI testing.**
### GOALS to achieve
* Run tests using just one command line.( Starting selenium server and run a suite against)
* Create an ouput useful for Jenkins.
* Tests have to be easy to change and fast to run.

###TECNOLOGY USED

* Node
* Nightwatch (npm install nightwatch --save) 

###NIGHTWATCH CONFIG

* The config file for nightwatch is nightwatch.json.
+ Most important things to configure in this json config file in order to acomplish the goals are:
    * _src_folders -> Path to the tests folder.
    * _output_folder -> Path to the reports folder._
    * _custom_commands_path -> Path to the generic functions used within the tests._
    * _selenium -> Selenium server config_
    * _start_process equals true -> Telling nightwatch we want to automatically start selenium._
    * _server_path -> Path to the selenium jar._
* test_settings
  _default -> Selenium connection config (localhost as we are starting selenium server)_
_desiredCapabilities -> Browser we use and basic capabilities._
_globals -> Our own variables to be used within the tests._

###COMMANDS
**nightwatch (Basic, runs all tests)**

* tag all (runs only tests tagged with all)
* reporter reporters/htmlReporter.js (creates an html report)