var fs = require('fs');
var compile = require('string-template/compile');

var buildReport = module.exports = {};

buildReport.build = function(results){
	var date = new Date();
	var dateString = (date.getDate()) + '.' + (date.getMonth()+1) + '.' + date.getFullYear() + ' ' + date.getHours() + ':' + date.getMinutes();
	var body = buildReport.t_testsPassed(results.tests, results.passed, results.failed);
	
	for(var result in results.modules){
		body = body + buildReport.t_test_title(result);
		var moduleTest = results.modules[result];
	
		for(var test_name in moduleTest.completed){
		
			var test = moduleTest.completed[test_name];
			
			if(test.failed > 0)
				body = body + buildReport.t_test_info("fail","&times;", test_name);
			else
				body = body + buildReport.t_test_info("success", "&radic;", test_name);
		}
		for(var test_name in moduleTest.skipped){
			body = body + buildReport.t_test_info("skipped", test_name);
		}
	}
	return '<!DOCTYPE html><head><link rel="stylesheet" type="text/css" href="ui_test.css"/></head><html><div class="title">APPLEGATE UI TEST RESULTS</div>'
	+ '<div class="last_run"> Last run: ' + dateString + '</div>'
	+ body + '<body></body></html>';
};

buildReport.t_testsPassed = compile('<div class="global_results"><p><b>Tests run: </b> {0} | <b>Tests passed: </b> {1} | <b>Tests failed: </b> {2}</p></div> ');
buildReport.t_test_title = compile('<div class="suite_title"><p><b>Test Suite: {0} </b></p></div>');
buildReport.t_test_info = compile('<div class="{0}"><p><b>{1} &nbsp; {2} </b></p></div>');


