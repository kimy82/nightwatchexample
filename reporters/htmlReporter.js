var fs = require('fs');
var reportBuilder = require('./reportBuilder.js');

var fileName = 'UI_TEST.html';
var stream = fs.createWriteStream(fileName);

module.exports = {
	write: function(results, options, done){
		stream.once('open', function(fd){
			stream.end(reportBuilder.build(results));
			done();
		});
	}
};

